package com.student.studentinfo.domain;

import java.util.Arrays;

/*

 @Author melone
 @Date 6/21/18 
 
 */

/**
 * It is a bean class that will be injected through student-config.xml file
 * this class is responsible for sorting and printing students
 */
public class Student {
    private String[] students;

    /**
     * String array of students is received from main class
     * @see com.student.studentinfo.StudentDemo
     * the students param is assigned to instance variable students through this keywore
     * @param students
     * after instantiating students[] printStudent is called to sort and print students
     */
    public void setStudents(String[] students) {
        this.students = students;
    }

    /**
     * @ TODO: 6/21/18  maintain method responsibility
     * uses java8 to sort the Strijng array
     * Uses method reference to print sorted array of Students
     */
    public void showStudent(){
        System.out.println("Sorted list of Students in ascending order");
        Arrays.stream(this.students).sorted().forEach(System.out::println);
    }
}
