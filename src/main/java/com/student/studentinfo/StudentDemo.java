package com.student.studentinfo;

import com.student.studentinfo.domain.Student;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.util.Scanner;

/*

 @Author melone
 @Date 6/21/18 
 
 */

/**
 * StudentDemo is the main method
 * It stores the student name entered by user to String array of students
 * @see com.student.studentinfo.domain.Student to see student bean class
 * ApplicationContext container has been used to maintain the lifecycle of bean
 */
public class StudentDemo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of students:");
        int num = sc.nextInt();
        String arrStudent[] = new String[num];
        System.out.println("Enter name of first " + num + "students");
        for (int i = 0; i < arrStudent.length; i++) {
            arrStudent[i] = sc.next();
        }
        ApplicationContext context = new ClassPathXmlApplicationContext("student-config.xml");
        Student student = ((Student) context.getBean("student"));
       // student.setStudents(arrStudent);
        student.showStudent();

    }
}